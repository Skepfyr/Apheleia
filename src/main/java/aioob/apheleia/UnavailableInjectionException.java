package aioob.apheleia;

/**
 * Thrown when the injection system is required to inject an object which it does not have.
 */
@SuppressWarnings("WeakerAccess")
public final class UnavailableInjectionException extends RuntimeException {

    private static final long serialVersionUID = 8444183731738671553L;

    /**
     * Constructs a new {@code UnavailableInjectionException} where the specified type is the one that was not available
     * to the injection system.
     *
     * @param type the unavailable type
     */
    public UnavailableInjectionException(Class type) {
        super(type.getSimpleName() + " was not available to be injected.");
    }
}
