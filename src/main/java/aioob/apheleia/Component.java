package aioob.apheleia;

/**
 * A component as in ECS architecture, components should just be data stores and not inherit from other components.
 */
public interface Component {
}
