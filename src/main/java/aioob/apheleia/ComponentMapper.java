package aioob.apheleia;

import java.util.HashMap;

/**
 * A utility for getting and setting a specific type of component on entities.
 *
 * @param <T> the type of {@code Component} that this maps
 */
public final class ComponentMapper<T extends Component> {

    private static int nextID = 0;

    /** The id of the component type, used as succinct identity to for a flag in a bit set structure. */
    final int id = nextID++;
    private final HashMap<Integer, T> components = new HashMap<>();
    private World world;

    /**
     * Used to set the world reference as it is not created at the time this is constructed.
     *
     * @param world the world this belongs to
     */
    void setWorld(World world) {
        this.world = world;
    }

    /**
     * Gets the component that is attached to the supplied entity.
     *
     * @param entity the entity to fetch the component from
     * @return The component or null if the entity does not have a component of the type of this mapper
     */
    public T get(int entity) {
        return this.components.get(entity);
    }

    /**
     * Attaches the specified component to the supplied entity.
     *
     * @param entity    the entity to attach the component to
     * @param component the component to add to the entity
     */
    public void set(int entity, T component) {
        this.components.put(entity, component);
        this.world.setComponentExists(entity, this.id, true);

        // Notify all the listeners
        for (EntityListener listener : this.world.entityListeners) {
            listener.onAddComponent(entity, component);
        }
    }

    /**
     * Removes the component of this type from the specified entity.
     *
     * @param entity the entity to remove the component from
     */
    public void remove(int entity) {
        Component component = this.components.remove(entity);
        if (component != null) {
            this.world.setComponentExists(entity, this.id, false);

            // Notify all the listeners
            for (EntityListener listener : this.world.entityListeners) {
                listener.onRemoveComponent(entity, component);
            }
        }
    }

    /**
     * Verifies if the supplied entity has a component of this type attached to it.
     *
     * @param entity the entity to check
     * @return {@code true} if the entity has a component of this type, {@code false} otherwise
     */
    public boolean has(int entity) {
        return this.world.doesComponentExist(entity, this.id);
    }
}
