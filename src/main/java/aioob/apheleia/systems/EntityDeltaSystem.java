package aioob.apheleia.systems;

import aioob.apheleia.ComponentMatcher;

/**
 * An {@code EntitySystem} that follows the contract of {@code DeltaSystem} so is called as frequently as possible by
 * the world.
 */
public abstract class EntityDeltaSystem extends EntitySystem implements DeltaSystem {

    /**
     * Constructs an {@code EntityDeltaSystem} which operates on the entities as specified by the provided
     * {@link ComponentMatcher}.
     *
     * @param componentMatcherBuilder a builder configured to match the entities that this should operate on
     */
    public EntityDeltaSystem(ComponentMatcher.Builder componentMatcherBuilder) {
        super(componentMatcherBuilder);
    }

    /**
     * Runs before this system starts iterating through its entities, used for setup.
     *
     * @param delta             the world time since the last invocation of this method
     * @param timeSinceLastTick the world time elapsed since the last tick
     */
    @SuppressWarnings("WeakerAccess")
    public void begin(float delta, float timeSinceLastTick) {
    }

    /**
     * Runs once per entity that this system operates on with the same values as was passed into
     * {@link #process(float, float) process(delta, timeSinceLastTick)}.
     *
     * @param entity            the entity it is operating on
     * @param delta             the world time since the last invocation of this method
     * @param timeSinceLastTick the world time elapsed since the last tick
     */
    @SuppressWarnings("WeakerAccess")
    public abstract void process(int entity, float delta, float timeSinceLastTick);

    /**
     * Runs after this system stops iterating through its entities
     *
     * @param delta             the world time since the last invocation of this method
     * @param timeSinceLastTick the world time elapsed since the last tick
     */
    @SuppressWarnings("WeakerAccess")
    public void end(float delta, float timeSinceLastTick) {
    }

    @Override
    public void process(float delta, float timeSinceLastTick) {
        this.begin(delta, timeSinceLastTick);

        for (int entity = 0; entity < this.entities.length(); ++entity) {
            entity = this.entities.nextSetBit(entity);
            this.process(entity, delta, timeSinceLastTick);
        }

        this.end(delta, timeSinceLastTick);
    }
}
