package aioob.apheleia.systems;

import aioob.apheleia.World;

/**
 * A system as in ECS architecture, does very little by itself, see its subtypes for more useful properties.
 *
 * @see DeltaSystem
 * @see TickingSystem
 */
public interface System {

    /**
     * Initialises the world that this system is in, as the world will not have been created at the time of this
     * object's construction.
     *
     * @param world the world that this is in
     */
    default void initWorld(World world) {
    }

    /**
     * Disposes of this system cleanly, called when the world is being shut down.
     */
    default void dispose() {
    }
}
