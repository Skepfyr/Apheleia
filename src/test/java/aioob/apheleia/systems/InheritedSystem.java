package aioob.apheleia.systems;

import aioob.apheleia.ComponentMapper;
import aioob.apheleia.components.Component1;
import aioob.apheleia.components.Component2;

public class InheritedSystem extends PrivateComponent1System {
    private ComponentMapper<Component2> component2Mapper;

    public ComponentMapper<Component1> getComponent1Mapper() {
        return this.component1Mapper;
    }

    public ComponentMapper<Component2> getComponent2Mapper() {
        return this.component2Mapper;
    }
}

class PrivateComponent1System implements System {
    ComponentMapper<Component1> component1Mapper;
}
