package aioob.apheleia.systems;

import aioob.apheleia.ComponentMapper;
import aioob.apheleia.components.Component1;

public class PrivateSystem implements System {
    private PrivateSystem privateSystem;
    private ComponentMapper<Component1> component1Mapper;

    public PrivateSystem getPrivateSystem() {
        return this.privateSystem;
    }

    public ComponentMapper<Component1> getComponent1Mapper() {
        return this.component1Mapper;
    }
}
