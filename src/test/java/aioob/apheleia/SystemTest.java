package aioob.apheleia;

import aioob.apheleia.components.Component1;
import aioob.apheleia.systems.DeltaSystem;
import aioob.apheleia.systems.System;
import aioob.apheleia.systems.TickingSystem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class SystemTest {

    @Mock(extraInterfaces = {EntityListener.class})
    private System system;
    @Mock(extraInterfaces = {EntityListener.class})
    private DeltaSystem deltaSystem;
    @Mock(extraInterfaces = {EntityListener.class})
    private TickingSystem tickingSystem;
    private World world;

    @Before
    public void before() {
        reset(this.system, this.deltaSystem, this.tickingSystem);
        this.world = new WorldBuilder().add(this.system).add(this.deltaSystem).add(this.tickingSystem)
                .requireComponent(Component1.class).build();
    }

    @Test
    public void initWorld() {
        verify(this.system).initWorld(this.world);
        verify(this.deltaSystem).initWorld(this.world);
        verify(this.tickingSystem).initWorld(this.world);
    }

    @Test
    public void deltaProcessing() {
        for (int i = 0; i < 10; i++) {
            this.world.process(0.1f * i);
        }
        InOrder inOrder = inOrder(this.deltaSystem);
        float sum = 0f;
        for (int i = 0; i < 10; i++) {
            sum += 0.1f * i;
            inOrder.verify(this.deltaSystem).process(0.1f * i, sum);
            sum = sum > 1f ? sum - 1f : sum;
        }
        inOrder.verify(this.deltaSystem, never()).process(anyFloat(), anyFloat());
    }

    @Test
    public void tickingProcessing() {
        for (int i = 0; i < 10; i++) {
            this.world.process(0.1f * i);
        }
        verify(this.tickingSystem, times(4)).process();
    }

    @Test
    public void disposalTest() {
        this.world.dispose();
        verify(this.system).dispose();
        verify(this.deltaSystem).dispose();
        verify(this.tickingSystem).dispose();
    }

    @Test
    public void createListenerTest() {
        int entity = this.world.create();
        verify((EntityListener) this.system).onCreateEntity(entity);
        verify((EntityListener) this.deltaSystem).onCreateEntity(entity);
        verify((EntityListener) this.tickingSystem).onCreateEntity(entity);
    }

    @Test
    public void deleteListenerTest() {
        int entity = this.world.create();
        this.world.delete(entity);
        verify((EntityListener) this.system).onDeleteEntity(entity);
        verify((EntityListener) this.deltaSystem).onDeleteEntity(entity);
        verify((EntityListener) this.tickingSystem).onDeleteEntity(entity);
    }

    @Test
    public void createComponentListenerTest() {
        int entity = this.world.create();
        Component1 component = new Component1();
        this.world.getMapper(Component1.class).set(entity, component);
        verify((EntityListener) this.system).onAddComponent(entity, component);
        verify((EntityListener) this.deltaSystem).onAddComponent(entity, component);
        verify((EntityListener) this.tickingSystem).onAddComponent(entity, component);
    }

    @Test
    public void deleteComponentListenerTest() {
        int entity = this.world.create();
        ComponentMapper<Component1> mapper = this.world.getMapper(Component1.class);
        Component1 component = new Component1();
        mapper.set(entity, component);
        mapper.remove(entity);
        verify((EntityListener) this.system).onRemoveComponent(entity, component);
        verify((EntityListener) this.deltaSystem).onRemoveComponent(entity, component);
        verify((EntityListener) this.tickingSystem).onRemoveComponent(entity, component);
    }

    @Test
    public void deleteNonComponentListenerTest() {
        int entity = this.world.create();
        ComponentMapper<Component1> mapper = this.world.getMapper(Component1.class);
        mapper.remove(entity);
        verify((EntityListener) this.system, never()).onRemoveComponent(eq(entity), any());
        verify((EntityListener) this.deltaSystem, never()).onRemoveComponent(eq(entity), any());
        verify((EntityListener) this.tickingSystem, never()).onRemoveComponent(eq(entity), any());
    }
}
