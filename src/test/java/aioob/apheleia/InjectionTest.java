package aioob.apheleia;

import aioob.apheleia.components.Component1;
import aioob.apheleia.systems.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class InjectionTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void publicInjection() {
        PublicSystem system = new PublicSystem();
        new WorldBuilder().add(system).build();
        assertEquals(system, system.publicSystem);
        assertNotNull(system.component1Mapper);
    }

    @Test
    public void extraComponentInjection() {
        World world = new WorldBuilder().requireComponent(Component1.class).build();
        assertNotNull(world.getMapper(Component1.class));
    }

    @Test
    public void privateInjection() {
        PrivateSystem system = new PrivateSystem();
        new WorldBuilder().add(system).build();
        assertEquals(system, system.getPrivateSystem());
        assertNotNull(system.getComponent1Mapper());
    }

    @Test
    public void inheritanceInjection() {
        InheritedSystem system = new InheritedSystem();
        new WorldBuilder().add(system).build();
        assertNotNull(system.getComponent1Mapper());
        assertNotNull(system.getComponent2Mapper());
    }

    @Test
    public void cyclicSystemInjection() {
        CyclicSystemA systemA = new CyclicSystemA();
        CyclicSystemB systemB = new CyclicSystemB();
        new WorldBuilder().add(systemA).add(systemB).build();
        assertEquals(systemA, systemB.cyclicSystemA);
        assertEquals(systemB, systemA.cyclicSystemB);
    }

    @Test
    public void managerInjection() {
        TagManager tagManager = new TagManager();
        TagManagerSystem system = new TagManagerSystem();
        new WorldBuilder().add(tagManager).add(system).build();
        assertEquals(tagManager, system.tagManager);
    }

    @Test
    public void unavailableInjection() {
        this.thrown.expect(UnavailableInjectionException.class);
        this.thrown.expectMessage("TagManager was not available to be injected.");
        TagManagerSystem system = new TagManagerSystem();
        new WorldBuilder().add(system).build();
    }

    @Test
    public void identicalSystemInjection() {
        this.thrown.expect(IllegalArgumentException.class);
        this.thrown.expectMessage("A system of type PublicSystem has already been added.");
        new WorldBuilder().add(new PublicSystem()).add(new PublicSystem()).build();
    }
}
