package aioob.apheleia;

import aioob.apheleia.components.Component1;
import aioob.apheleia.components.Component2;
import aioob.apheleia.components.Component3;
import aioob.apheleia.components.Component4;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ComponentMatcherTest {

    private final World world = new WorldBuilder().requireComponent(Component1.class).requireComponent(Component2.class)
            .requireComponent(Component3.class).requireComponent(Component4.class).build();
    private final ComponentMapper<Component1> component1Mapper = this.world.getMapper(Component1.class);
    private final ComponentMapper<Component2> component2Mapper = this.world.getMapper(Component2.class);
    private final ComponentMapper<Component3> component3Mapper = this.world.getMapper(Component3.class);
    private final ComponentMapper<Component4> component4Mapper = this.world.getMapper(Component4.class);
    private int entity;

    @Before
    public void before() {
        this.entity = this.world.create();
    }

    @Test
    public void allMatch() {
        this.component1Mapper.set(this.entity, new Component1());
        this.component2Mapper.set(this.entity, new Component2());
        this.component3Mapper.set(this.entity, new Component3());
        assertTrue(ComponentMatcher.all(Component1.class, Component2.class).build(this.world).matches(this.entity));
    }

    @Test
    public void allNonMatch() {
        this.component1Mapper.set(this.entity, new Component1());
        this.component2Mapper.set(this.entity, new Component2());
        assertFalse(ComponentMatcher.all(Component1.class, Component2.class, Component3.class)
                .build(this.world).matches(this.entity));
    }

    @Test
    public void anyMatch() {
        this.component1Mapper.set(this.entity, new Component1());
        this.component2Mapper.set(this.entity, new Component2());
        this.component3Mapper.set(this.entity, new Component3());
        assertTrue(ComponentMatcher.any(Component3.class, Component4.class).build(this.world).matches(this.entity));
    }

    @Test
    public void anyNonMatch() {
        this.component1Mapper.set(this.entity, new Component1());
        this.component2Mapper.set(this.entity, new Component2());
        assertFalse(ComponentMatcher.any(Component3.class, Component4.class).build(this.world).matches(this.entity));
    }

    @Test
    public void notMatch() {
        this.component1Mapper.set(this.entity, new Component1());
        this.component2Mapper.set(this.entity, new Component2());
        assertTrue(ComponentMatcher.not(Component3.class, Component4.class).build(this.world).matches(this.entity));
    }

    @Test
    public void notNonMatch() {
        this.component1Mapper.set(this.entity, new Component1());
        this.component2Mapper.set(this.entity, new Component2());
        this.component3Mapper.set(this.entity, new Component3());
        assertFalse(ComponentMatcher.not(Component3.class, Component4.class).build(this.world).matches(this.entity));
    }

    @Test
    public void complexMatch() {
        this.component1Mapper.set(this.entity, new Component1());
        this.component2Mapper.set(this.entity, new Component2());
        this.component3Mapper.set(this.entity, new Component3());
        assertTrue(ComponentMatcher.all(Component1.class, Component2.class)
                .any(Component3.class, Component4.class)
                .not(Component4.class).build(this.world).matches(this.entity));
    }

    @Test
    public void complexNonMatch() {
        this.component1Mapper.set(this.entity, new Component1());
        this.component3Mapper.set(this.entity, new Component3());
        this.component4Mapper.set(this.entity, new Component4());
        assertFalse(ComponentMatcher.all(Component1.class, Component2.class)
                .any(Component3.class, Component4.class)
                .not(Component4.class).build(this.world).matches(this.entity));
    }
}
