# Apheleia [![build status](https://gitlab.com/AIOOB/Apheleia/badges/master/build.svg)](https://gitlab.com/AIOOB/Apheleia/commits/master) [![coverage report](https://gitlab.com/AIOOB/Apheleia/badges/master/coverage.svg)](https://gitlab.com/AIOOB/Apheleia/commits/master)

A Java ECS written for ease-of-use, simplicity, then speed, in that order (mostly).
